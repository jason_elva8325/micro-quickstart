package common

import (
	"errors"
	"net"
)

var (
	errNonNetwork = errors.New("There is no network address or interface not active")
)

// NetInterfaceCheck 本机IPV4地址获取方法
func NetInterfaceCheck() (string, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", errNonNetwork
	}
	for _, address := range addrs {
		// 检查ip地址判断是否回环地址, IPV4是否存在
		ipnet, ok := address.(*net.IPNet)
		if ok && !ipnet.IP.IsLoopback() && ipnet.IP.To4() != nil && ipnet.IP.String() != "" {
			return ipnet.IP.String(), nil
		}
	}
	return "", errNonNetwork
}
