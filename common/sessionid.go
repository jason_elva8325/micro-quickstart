package common

// 自定义metadata上下文变量类型
type contextKey string

// ContextKey 全局context变量Key值
//   sessionID表示服务链路上一次完成的请求
//   一般从API网关产生，保证每次请求唯一，并在服务链路上始终保持传输
//   可用于服务链路追踪使用
const ContextKey contextKey = "sessionid"
