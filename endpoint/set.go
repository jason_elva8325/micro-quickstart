package endpoint

import (
	"context"
	"time"

	"gitee.com/jason_elva8325/micro-quickstart/service"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
	"github.com/go-kit/kit/ratelimit"
	"golang.org/x/time/rate"
)

// Set 服务端点集合定义
type Set struct {
	SayHelloEndpoint  endpoint.Endpoint
	SayGoodbyEndpoint endpoint.Endpoint
}

// New 新建服务端点方法
func New(svc service.Service, logger log.Logger, duration metrics.Histogram, rateLimit int) Set {
	var sayHelloEndpoint endpoint.Endpoint
	{
		sayHelloEndpoint = MakeSayHelloEndpoint(svc)
		if rateLimit > 0 {
			sayHelloEndpoint = ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Every(time.Second), rateLimit))(sayHelloEndpoint) // 限流错误返回，服务直接返回超限错误
		}
		sayHelloEndpoint = LoggingMiddleware(log.With(logger, "method", "SayHello"))(sayHelloEndpoint)
		sayHelloEndpoint = InstrumentingMiddleware(duration.With("method", "SayHello"))(sayHelloEndpoint)
	}

	var sayGoodbyEndpoint endpoint.Endpoint
	{
		sayGoodbyEndpoint = MakeSayGoodbyEndpoint(svc)
		if rateLimit > 0 {
			sayGoodbyEndpoint = ratelimit.NewDelayingLimiter(rate.NewLimiter(rate.Every(time.Second), rateLimit))(sayGoodbyEndpoint) // 限流延迟访问，服务延时返回结果
		}
		sayGoodbyEndpoint = LoggingMiddleware(log.With(logger, "method", "SayGoodby"))(sayGoodbyEndpoint)
		sayGoodbyEndpoint = InstrumentingMiddleware(duration.With("method", "SayGoodby"))(sayGoodbyEndpoint)
	}

	return Set{
		SayHelloEndpoint:  sayHelloEndpoint,
		SayGoodbyEndpoint: sayGoodbyEndpoint,
	}
}

// SayHello 服务端点的业务方法定义，一般情况下直接同名透传到服务层
func (s Set) SayHello(ctx context.Context, lang, target string) (string, error) {
	resp, err := s.SayHelloEndpoint(ctx, SayHelloRequest{lang, target})
	if err != nil {
		return "", err
	}
	response := resp.(SayHelloResponse)
	return response.V, response.Err
}

// SayGoodby 服务端点的业务方法定义，一般情况下直接同名透传到服务层
func (s Set) SayGoodby(ctx context.Context, lang, target string) (string, error) {
	resp, err := s.SayGoodbyEndpoint(ctx, SayGoodbyRequest{lang, target})
	if err != nil {
		return "", err
	}
	response := resp.(SayGoodbyResponse)
	return response.V, response.Err
}

// MakeSayHelloEndpoint 创建服务端点方法
func MakeSayHelloEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(SayHelloRequest)
		result, err := svc.SayHello(ctx, req.Lang, req.Target)
		return SayHelloResponse{V: result, Err: err}, nil
	}
}

// MakeSayGoodbyEndpoint 创建服务端点方法
func MakeSayGoodbyEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(SayGoodbyRequest)
		result, err := svc.SayGoodby(ctx, req.Lang, req.Target)
		return SayGoodbyResponse{V: result, Err: err}, nil
	}
}

// 实现 endpoint.Failer 接口的响应类型断言
var (
	_ endpoint.Failer = SayHelloResponse{}
	_ endpoint.Failer = SayGoodbyResponse{}
)

// SayHelloRequest 服务端点请求对象定义
type SayHelloRequest struct {
	Lang   string
	Target string
}

// SayHelloResponse 服务端点响应对象定义
type SayHelloResponse struct {
	V   string `json:"V"`
	Err error  `json:"-"` // 将由失败/错误编码器拦截
}

// Failed 服务端点的相应对象需要继承 endpoint.Failer 接口
func (r SayHelloResponse) Failed() error { return r.Err }

// SayGoodbyRequest 服务端点请求对象定义
type SayGoodbyRequest struct {
	Lang   string
	Target string
}

// SayGoodbyResponse 服务端点响应对象定义
type SayGoodbyResponse struct {
	V   string `json:"V"`
	Err error  `json:"-"` // 将由失败/错误编码器拦截
}

// Failed 服务端点的相应对象需要继承 endpoint.Failer 接口
func (r SayGoodbyResponse) Failed() error { return r.Err }
