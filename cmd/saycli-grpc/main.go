package main

import (
	"context"
	"errors"
	"flag"
	"io"
	"os"
	"strings"
	"time"

	"google.golang.org/grpc/metadata"

	"gitee.com/jason_elva8325/micro-quickstart/pb"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/etcdv3"
	"github.com/go-kit/kit/sd/lb"
	"google.golang.org/grpc"
)

var (
	srdETCDServers                                              = flag.String("srd-etcd-servers", "127.0.0.1:2379", "Comma-separated ETCD server addresses for Service Discovery & Regist. Example: '127.0.0.1:2379,127.0.0.1:13279'")
	srdETCDCaCert                                               = flag.String("srd-etcd-ca-cert", "", "ETCD client access root cert file path for Service Discovery & Registry")
	srdETCDCert                                                 = flag.String("srd-etcd-cert", "", "ETCD client access cert file path for Service Discovery & Registry")
	srdETCDKey                                                  = flag.String("srd-etcd-key", "", "ETCD client access cert's key file path for Service Discovery & Registry")
	srdPrefix                                                   = flag.String("srd-prefix", "/svc/", "Service Discovery & Registry Prefix")
	logger                                                      log.Logger
	paramLang, paramTarget, paramFunc, paramSvc, paramSessionID string
)

func init() {
	logger = log.NewJSONLogger(os.Stderr)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = log.With(logger, "caller", log.DefaultCaller)

	flag.StringVar(&paramSvc, "paramSvc", "Say", "Service Parameter")
	flag.StringVar(&paramFunc, "paramFunc", "SayHello", "Function Parameter")
	flag.StringVar(&paramLang, "paramLang", "zh", "Lang Parameter")
	flag.StringVar(&paramTarget, "paramTarget", "世界", "Target Parameter")
	flag.StringVar(&paramSessionID, "paramSessionID", "demoSessionID", "Session ID")
}

func main() {
	flag.Parse()

	client, err := etcdv3.NewClient(context.Background(), strings.Split(*srdETCDServers, ","), etcdv3.ClientOptions{
		DialTimeout:   time.Second * 3,
		DialKeepAlive: time.Second * 3,
		CACert:        *srdETCDCaCert,
		Cert:          *srdETCDCert,
		Key:           *srdETCDKey,
	})
	if err != nil {
		logger.Log("Service regist to ETCD server fail", err)
		os.Exit(1)
	}

	//创建实例例管理理器器, 此管理理器器会Watch监听etc中prefix的⽬目录变化更更新缓存的服务实例例数据
	instancer, err := etcdv3.NewInstancer(client, *srdPrefix+paramSvc+"/GRPC/", logger)
	if err != nil {
		logger.Log("Service watcher on ETCD server create fail", err)
		os.Exit(1)
	}
	//创建端点管理理器器， 此管理理器器根据Factory和监听的到实例例创建endPoint并订阅instancer的变化
	endpointer := sd.NewEndpointer(instancer, reqFactory, logger)
	//创建负载均衡器器
	balancer := lb.NewRoundRobin(endpointer)
	reqEndPoint := lb.Retry(2, 3*time.Second, balancer)
	//创建服务熔断
	// 可通过关闭服务端予以测试
	commandName := "my-endpoint"
	hystrix.ConfigureCommand(commandName, hystrix.CommandConfig{
		Timeout:                100,   // 请求超时的时间
		ErrorPercentThreshold:  50,    // 允许出现的错误比例
		SleepWindow:            10000, // 熔断开启多久尝试发起一次请求
		MaxConcurrentRequests:  1000,  // 允许的最大并发请求数
		RequestVolumeThreshold: 5,     // 波动期内的最小请求数，默认波动期 10S
	})
	reqEndPoint = circuitbreaker.Hystrix(commandName)(reqEndPoint)
	//通过 endPoint 发起请求
	req := struct{}{}
	t := time.NewTicker(time.Millisecond * 40)
	for range t.C {
		res, err := reqEndPoint(context.Background(), req)
		if err != nil {
			logger.Log("Service discovery from ETCD server fail", err)
		}
		switch rt := res.(type) {
		case *pb.SayHelloResponse:
			logger.Log("Service call", "Finished", "Result", rt.V)
		case *pb.SayGoodbyResponse:
			logger.Log("Service call", "Finished", "Result", rt.V)
		default:
			logger.Log("Service call", "No Reply")
		}
	}
}

func reqFactory(instanceAddr string) (endpoint.Endpoint, io.Closer, error) {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		logger.Log("grpc dail up addr", instanceAddr)

		var err error
		var rs interface{}

		conn, err := grpc.Dial(instanceAddr, grpc.WithInsecure(), grpc.WithTimeout(time.Second))
		if err != nil {
			logger.Log("grpc dail error", err)
			os.Exit(1)
		}
		defer conn.Close()

		// 追加sessionid到GRPC的metadata信息中，并返回修改后的context，用于服务链路追踪
		ctx = metadata.AppendToOutgoingContext(ctx, "sessionid", paramSessionID)

		client := pb.NewSayServiceClient(conn)

		switch paramFunc {
		case "SayHello":
			rs, err = client.SayHello(ctx, &pb.SayHelloRequest{
				Lang:   paramLang,
				Target: paramTarget,
			})
		case "SayGoodby":
			rs, err = client.SayGoodby(ctx, &pb.SayGoodbyRequest{
				Lang:   paramLang,
				Target: paramTarget,
			})
		default:
			err = errors.New("No such Function")
		}

		if err != nil {
			logger.Log("Function", paramFunc, "Execute", "fail")
			return nil, err
		}
		logger.Log("Function", paramFunc, "Execute", "success")
		return rs, nil
	}, nil, nil
}
