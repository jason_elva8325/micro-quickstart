#!/bin/sh
dstDir=$(cd `dirname $0`; pwd)
protoc -I=$dstDir --go_out=plugins=grpc:$dstDir $dstDir/*.proto
