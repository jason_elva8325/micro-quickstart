# micro-quickstart

#### 项目介绍
基于 [go-kit](https://github.com/go-kit/kit) 微服务工具集，实现基于 gRPC 协议或 RestFul 协议的微服务样例。

包含：服务实现、服务日志、服务度量、服务熔断、服务限流、服务注册发现、服务链路追踪

#### 软件架构
- common (服务通用组件)
- config (TOML 格式配置文件解析组件)
- pb (gRPC 服务定义)
- service (服务实现)
- endpoint (服务端点定义)
- transport (服务传输协议定义)
- cmd
  - saysvc-grpc (服务端指令)
  - saysvc-http (服务端指令)
  - saycli-grpc (客户端指令)
  - saycli-http (客户端指令)

#### 软件流程图

![bootstrap](bootstrap.png)

![access_flow](access_flow.png)

#### 安装方法

git clone https://gitee.com/jason_elva8325/micro-quickstart.git

#### 命令及参数

```sh
# gRPC 服务端

Usage of saysvc-grpc:
  -conf-file string
    	Host path of TOML format config file. If this param set, other params will be lose efficacy
  -debug-port string
    	Debug and metrics listen port (default "9000")
  -grpc-port string
    	gRPC listen port (default "8082")
  -listen-host string
    	TCP listen host (default "0.0.0.0")
  -rate-limit int
    	Times of service can be call per second. Less than or equal to zero means no limit
  -srd-etcd-ca-cert string
    	ETCD client access root cert file path for Service Discovery & Registry
  -srd-etcd-cert string
    	ETCD client access cert file path for Service Discovery & Registry
  -srd-etcd-key string
    	ETCD client access cert‘s key file path for Service Discovery & Registry
  -srd-etcd-servers string
    	Comma-separated ETCD server addresses for Service Discovery & Registry. Example: '127.0.0.1:2379,127.0.0.1:13279' (default "127.0.0.1:2379")
  -srd-prefix string
    	Service Discovery & Registry Prefix (default "/svc/")
  -svc-name string
    	Service Name, use to unique service type (default "Say")

# RestFul 服务端

Usage of saysvc-http:
  -conf-file string
    	Host path of TOML format config file. If this param set, other params will be lose efficacy
  -debug-port string
    	Debug and metrics listen port (default "9000")
  -http-port string
    	HTTP listen port (default "8081")
  -listen-host string
    	TCP listen host (default "0.0.0.0")
  -rate-limit int
    	Times of service can be call per second. Less than or equal to zero means no limit
  -srd-etcd-ca-cert string
    	ETCD client access root cert file path for Service Discovery & Registry
  -srd-etcd-cert string
    	ETCD client access cert file path for Service Discovery & Registry
  -srd-etcd-key string
    	ETCD client access cert‘s key file path for Service Discovery & Registry
  -srd-etcd-servers string
    	Comma-separated ETCD server addresses for Service Discovery & Registry. Example: '127.0.0.1:2379,127.0.0.1:13279' (default "127.0.0.1:2379")
  -srd-prefix string
    	Service Discovery & Registry Prefix (default "/svc/")
  -svc-name string
    	Service Name, use to unique service type (default "Say")
```

##### 使用说明

1. -conf-file 参数如果一旦设定，则其它参数均不起作用，TOML 配置文件样例，详见 config 目录
2. -srd-etcd-* 参数必须一并设置，否则不能连接 ETCD 服务器

#### Command 构建脚本说明

使用 cmd 目录下的 gen.sh 脚本构建命令文件。该脚本的使用方法如下：`gen.sh all` 代表构建所有 Command，`gen.sh [command 目录名]` 代表构建指定的 command，构建生成的命令文件名同 `[command 目录名]`

#### 依赖条件

- ETCD 服务作为注册发现服务
- prometheus 作为服务度量服务

#### 其它说明

本实例内的服务链路追踪，是以从 API 网关开始，到最终返回 API 网关定义为一次完整的请求响应。以 API 网关在请求报文头中产生的 sessionid 为追踪 ID，通过在服务链路中各服务按照时序输出的日志信息作为追踪信息实现