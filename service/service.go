package service

import (
	"context"
	"errors"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
)

// Service 服务接口定义
type Service interface {
	SayHello(ctx context.Context, lang, target string) (string, error)
	SayGoodby(ctx context.Context, lang, target string) (string, error)
}

// New 新建服务方法，包含对非功能性中间件的添加
func New(logger log.Logger, successInts, failInts metrics.Counter) Service {
	var svc Service
	{
		svc = NewBasicService()
		svc = LoggingMiddleware(logger)(svc)
		svc = InstrumentingMiddleware(successInts, failInts)(svc)
	}
	return svc
}

// 自定义错误类型
var (
	ErrInvalidLang = errors.New("Invalid Lang for output")
)

type basicService struct{}

// NewBasicService 新建服务方法，仅实现业务自身部分
func NewBasicService() Service {
	return basicService{}
}

func (s basicService) SayHello(_ context.Context, lang, target string) (string, error) {
	var v string
	var err error

	switch lang {
	case "zh":
		v = "你好，" + target + "!"
	case "jp":
		v = "こんにちは、" + target + "!"
	case "ko":
		v = "안녕, " + target + "!"
	case "en":
		v = "Hello, " + target + "!"
	case "fr":
		v = "Bonjour, " + target + "!"
	case "es":
		v = "Hola, " + target + "!"
	case "ru":
		v = "Привет," + target + "!"
	default:
		err = ErrInvalidLang
	}
	return v, err
}

func (s basicService) SayGoodby(_ context.Context, lang, target string) (string, error) {
	var v string
	var err error

	switch lang {
	case "zh":
		v = "再见，" + target + "!"
	case "jp":
		v = "さようなら、" + target + "!"
	case "ko":
		v = "안녕히 계 세요, " + target + "!"
	case "en":
		v = "Goodby, " + target + "!"
	case "fr":
		v = "Au revoir, " + target + "!"
	case "es":
		v = "Adiós, " + target + "!"
	case "ru":
		v = "Пока," + target + "!"
	default:
		err = ErrInvalidLang
	}
	return v, err
}
