package service

import (
	"context"

	"gitee.com/jason_elva8325/micro-quickstart/common"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
)

// Middleware 中间件对象定义，需要实现服务的接口定义，并返回服务对象
type Middleware func(Service) Service

// LoggingMiddleware 日志中间件定义方法
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next Service) Service {
		return loggingMiddleware{logger, next}
	}
}

type loggingMiddleware struct {
	logger log.Logger // 日志服务对象
	next   Service    // 服务逻辑本身
}

func (mw loggingMiddleware) SayHello(ctx context.Context, lang, target string) (v string, err error) {
	defer func() {
		mw.logger.Log("method", "SayHello", "sessionid", ctx.Value(common.ContextKey).(string), "lang", lang, "target", target, "v", v, "err", err)
	}()
	return mw.next.SayHello(ctx, lang, target)
}

func (mw loggingMiddleware) SayGoodby(ctx context.Context, lang, target string) (v string, err error) {
	defer func() {
		mw.logger.Log("method", "SayGoodby", "sessionid", ctx.Value(common.ContextKey).(string), "lang", lang, "target", target, "v", v, "err", err)
	}()
	return mw.next.SayGoodby(ctx, lang, target)
}

// InstrumentingMiddleware 度量中间件定义方法
func InstrumentingMiddleware(helloInts, goodbyInts metrics.Counter) Middleware {
	return func(next Service) Service {
		return instrumentingMiddleware{
			helloInts:  helloInts,
			goodbyInts: goodbyInts,
			next:       next,
		}
	}
}

type instrumentingMiddleware struct {
	helloInts  metrics.Counter // 记录hello服务调用次数
	goodbyInts metrics.Counter // 记录googby服务调用次数
	next       Service         // 服务逻辑本身
}

func (mw instrumentingMiddleware) SayHello(ctx context.Context, lang, target string) (string, error) {
	v, err := mw.next.SayHello(ctx, lang, target)
	mw.helloInts.Add(1)
	return v, err
}

func (mw instrumentingMiddleware) SayGoodby(ctx context.Context, lang, target string) (string, error) {
	v, err := mw.next.SayGoodby(ctx, lang, target)
	mw.goodbyInts.Add(1)
	return v, err
}
